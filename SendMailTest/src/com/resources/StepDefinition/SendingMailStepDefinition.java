package StepDefinition;


import org.mavenproject.SendMailTest.util.Base;
import org.mavenproject.SendMailTest.util.PropertyReader;
import org.openqa.selenium.WebDriver;

import FunctionLibrary.FuctionLibrary;
import io.cucumber.java.After;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SendingMailStepDefinition {

	WebDriver driver;
	PropertyReader pr;

	FuctionLibrary library = new FuctionLibrary();
	Integer currSenderIdx = 0;
	Integer sendersListSize = 0;

	public SendingMailStepDefinition() {
		pr = new PropertyReader();
		driver = new Base().getDriver();
	}

	@When("^User navigate to yahoo with email (.*?) password (.*?) for ittr (.*?)$")
	public void navigateToYahoo(String email, String password, String ittrNum) {
		library.navigateToYahoo(driver, email, password, ittrNum);
	}

	@Then("User clicks on compose mail")
	public void gotToMail() throws InterruptedException {
		System.out.println("step definition");
		library.gotToMail(driver);

	}

	@Then("User Compose a mail")
	public void composeMail() throws InterruptedException {
		System.out.println("step definition");
		library.composeMail(driver);

	}

	@Then("User logout")
	public void logout() throws InterruptedException {
		System.out.println("step definition");
		library.logout(driver);
		currSenderIdx += 1;
	}

	@After
	public void DriverClose() throws InterruptedException {
		driver.close();
	}

}
