package StepDefinition;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		monochrome = true,
		features = "src/com/resources/FeatureFile/Login.feature",
		// glue = "resources.stepdefinition", //path of step definition
		plugin = {"pretty", "json:target/cucumber.html"}
		)

public class CucumberSuitRunner {}