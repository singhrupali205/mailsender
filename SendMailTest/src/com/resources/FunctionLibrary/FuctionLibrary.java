package FunctionLibrary;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.IOUtils;
import org.mavenproject.SendMailTest.util.PropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class FuctionLibrary {
	String FileContent;
	Integer senderCount = 1;
	PropertyReader pr = new PropertyReader();

	public void navigateToYahoo(WebDriver driver, String email, String pass, String ittrNum) {
		// System.out.println(getMailBodyFromFile());
		senderCount = Integer.parseInt(ittrNum);
		driver.manage().window().maximize();

		driver.get(pr.getLoginUrl());
		WebElement element = driver.findElement(By.id("login-username"));
		element.sendKeys(email);

		driver.findElement(By.id("login-signin")).click();
		driver.manage().timeouts().implicitlyWait(05, TimeUnit.SECONDS);
		driver.findElement(By.id("login-passwd")).sendKeys(pass);
		driver.findElement(By.id("login-signin")).click();

	}

	// For
	public void gotToMail(WebDriver driver) throws InterruptedException {
		System.out.println("in login page");
		driver.get(pr.getMailUrl());
	}

	public void composeMail(WebDriver driver) {
		ArrayList<ArrayList<String>> receiversList = getEmailPassList(pr.getReceiverFilePath());
		System.out.println(receiversList); // [ ['email1', 'pass'], ['email2', 'pass'] ]
		URL url = getClass().getClassLoader().getResource(pr.getAttachmentPath());
		File attachmentfile = new File(url.getFile());

		Iterator<ArrayList<String>> it = receiversList.iterator();
		Integer receiverCount = 1;
		while (it.hasNext()) {
			ArrayList<String> emailPass = it.next();// ['email2', 'pass']

			String email = emailPass.get(0);
			driver.findElement(By.cssSelector("[data-test-id=\"compose-button\"]")).click();
			driver.findElement(By.id("message-to-field")).sendKeys(email);
			String subject = senderCount + " " + pr.getName() + " " + receiverCount;
			driver.findElement(By.xpath("//*[@data-test-id='compose-subject']")).sendKeys(subject);
			driver.findElement(By.xpath("//*[@data-test-id='rte']"))
					.sendKeys(subject + System.lineSeparator() + getMailBodyFromFile());
			putSubjectInAttachment(subject);
			driver.findElement(By.cssSelector("[data-test-id=\"popover-container\"] input[type=\"file\"]"))
					.sendKeys(attachmentfile.getPath());
			driver.findElement(By.xpath("//*[@data-test-id='rte']")).click();
			driver.findElement(By.xpath("//*[@data-test-id='compose-send-button']")).click();
			receiverCount += 1;
		}

		logout(driver);
	}

	private String getMailBodyFromFile() {
		if (null != FileContent) {
			return FileContent;
		}
		FileContent = getStringFromResouce(pr.getMailBodyFilePath());
		return FileContent;
	}

	private String getStringFromResouce(String fileName) {
//		File file = new File(getClass().getClassLoader().getResource(fileName).getFile());
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
		try {
			String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
			inputStream.close();
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

//	[ ['email', 'pass'], ['email', 'pass'] ]

	// TODO: get resource path form params
	public ArrayList<ArrayList<String>> getEmailPassList(String filename) {
		ClassLoader clsLoader = getClass().getClassLoader();
		// get from params
		URL url = clsLoader.getResource(filename);
		File file = new File(url.getFile());
		Scanner sc = null;
		ArrayList<ArrayList<String>> listOfReceivers = new ArrayList<ArrayList<String>>();
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				ArrayList<String> emailPassList = extractEmailAndPass(sc.nextLine());
				// ['email', 'pass']
				listOfReceivers.add(emailPassList);
			}

			return listOfReceivers;

		} catch (FileNotFoundException err) {
			throw new Error(err.toString());
		} catch (Exception err) {
			throw new Error(err.toString());
		} finally {
			if (sc != null)
				sc.close();
		}
	}

	public ArrayList<String> extractEmailAndPass(String emailPass) { // example@email.com,pass

		ArrayList<String> emailPassList = new ArrayList<String>(); // []
		Integer commaIdx = emailPass.indexOf(','); // 16

		String email = emailPass.substring(0, commaIdx); // example@email.com
		String pass = emailPass.substring(commaIdx + 1); // pass

		emailPassList.add(email); // [example@email.com]
		emailPassList.add(pass); // [example@email.com, pass]
		return emailPassList;

	}

	public void putSubjectInAttachment(String subject) {
//		try {
//			File file = new File(getClass().getClassLoader().getResource(pr.getAttachmentPath()).getFile());
//			FileWriter writer = new FileWriter(getClass().getResource(pr.getAttachmentPath()).getFile());
//			writer.write(subject);
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		try (FileWriter fw = new FileWriter(pr.getAttachmentPath());
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)) {
			out.println(subject);
			out.close();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void logout(WebDriver driver) {
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//*[@id=\"ybar-inner-wrap\"]/div[2]/div/div[3]/div[1]/div")));
		action.click().build().perform();
		driver.findElement(By.xpath("//*[@id=\"ybarAccountMenuBody\"]/a[3]/span[2]")).click();
	}

}
