package org.mavenproject.SendMailTest.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {

	public WebDriver driver;
	public String browser;

	public Base() {
		PropertyReader pr = new PropertyReader();
		browser = pr.getBrowser();
	}

	public WebDriver getDriver() {
		if ("chrome".equals(browser)) {
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}
		return driver;
	}
}
