package org.mavenproject.SendMailTest.util;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

	private final String propertyFilePath = "src/com/java/org/mavenproject/SendMailTest/util/data.properties";
	private static Properties properties = new Properties();

	public PropertyReader() {
		

		try {
			System.out.println("###########GETTING BROWSER PROPERTY: " + properties.getProperty("browser"));
			FileReader reader = new FileReader(propertyFilePath);
			properties.load(reader);
		} catch (FileNotFoundException e) {
			System.out.println("Property file not found");
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public String getDriverPath() {
		return properties.getProperty("chromeDriverLocation");
	}

	public String getBrowser() {
		return properties.getProperty("browser");
	}

	public String getApplicationUrl() {
		 String url = properties.getProperty("url");
		 if(url != null) return url;
		 else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
	
	public String getMailUrl() {
		String mailUrl = properties.getProperty("mailUrl");
		if(mailUrl != null) return mailUrl;
		else throw new RuntimeException("mailUrl not specified in the Configuration.properties file.");
	}
	
	public String getLoginUrl() {
		String loginUrl = properties.getProperty("loginUrl");
		if(loginUrl != null) return loginUrl;
		else throw new RuntimeException("loginUrl not specified in the Configuration.properties file.");
	}
	
	public String getReceiverFilePath() {
		String receiver = properties.getProperty("receiver");
		if(receiver != null) return receiver;
		else throw new RuntimeException("receiver not specified in the Configuration.properties file.");
	}
	
	public String getSenderFilePath() {
		String sender = properties.getProperty("sender");
		if(sender != null) return sender;
		else throw new RuntimeException("sender not specified in the Configuration.properties file.");
	}
	
	public String getMailBodyFilePath() {
		String mailBody = properties.getProperty("mailBody");
		if(mailBody != null) return mailBody;
		else throw new RuntimeException("mailBody not specified in the Configuration.properties file.");
	}
	
	public String getName() {
		String name = properties.getProperty("name");
		if(name != null) return name;
		else throw new RuntimeException("name not specified in the Configuration.properties file.");
	}
	
	public String getAttachmentPath() {
		String attachment = properties.getProperty("attachment");
		if(attachment != null) return attachment;
		else throw new RuntimeException("attachment not specified in the Configuration.properties file.");
	}
	
	
}
