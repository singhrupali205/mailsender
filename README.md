# Setting up the environment before test

## Download the web driver 

- Get the required web driver from the [links](https://www.selenium.dev/documentation/en/webdriver/driver_requirements/#quick-reference) in the official docd by Selenium
- Then add the web driver to your system path.

## To add the web driver to your system path
### For Windows
`setx /m path "%path%;C:\WebDriver\bin\"`

### For MacOS and Linux
```export PATH=$PATH:/opt/WebDriver/bin >> ~/.profile```

## To run the project

Run ``` CucumberSuitRunner.java ```
